# Test project symfony

Для начала создайте базу данных `rent`(rent.sql), а затем импортируйте туда rent.sql.

Для запуска данного проекта, через консоль с установленным Symfony 4.2 перейти на проект,
а затем написать: php bin/console server:run.
Затем нужно перейти по указанном адресу и у вас всё заработает
В config/routes.yaml Находятся все пути.
Контроллеры находятся в папке src\Controller.
src\Repository - общение с базой данных.
src\Entity - классы (сущности) таблиц.
templates - все View