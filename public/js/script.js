$('.getfiltre').click(function () {
  var masInp = '';
  var Inp = $('.active');
  // Заполняем массив активный ключ =  значение фильтра
  for (var i = 0; i < Inp.length; i++){
    masInp += Inp[i].name + "=" + Inp[i].value + "&";
  }
  masInp += 'getOrders=1';

  $.ajax({
    method: "POST",
    url: "",
    dataType:   'json',
    async:      true,
    data: masInp,
    success: function(Orders){
        var table = '';
        for (var i = 0; i < Orders.length; i++){
          table += '<tr><td>' + Orders[i].car_name + '</td><td class="center">' +
              Orders[i].number_car + '</td><td>' + Orders[i].user_fio   + ', Серия: ' +  Orders[i].pasport_number.split(' ')[0]
              + 'Номер:' + Orders[i].pasport_number.split(' ')[1] + '</td><td>' + Orders[i].price
              +  '</td><td class="center">' + Orders[i].drive_start + '</td><td class="center">'
              + Orders[i].drive_end + '</td><td class="center">'+ Orders[i].point_start + '</td>'
              +   '</td><td class="center">'+ Orders[i].point_end + '</td></tr>';
          }
        $('tbody')[0].innerHTML = table;
    },
    error : function(xhr, textStatus, errorThrown) {

    }
  })
});

$('.show').click(function () {
  document.querySelector('.filter').style.display = 'block';
  $(this).css('display', 'none');
});

// Показывает поле ввода каждого фильтра
$('.openFiltre').click(function () {
  var check = $(this).is(':checked');
  var input = $(this).parent().children('input:last-child');
  input = (input.length <= 0) ? $(this).parent().children('select:last-child') : input;

  if (check)   {
    input.show();
    input.addClass("active")
  }
  else{
    input.hide();
    input.removeClass("active")
  }
});

//Удаление аренды
$('.del').click(function () {
  var order = $(this)[0].value;
  $(this).parent().parent().remove();
  $.ajax({
    method: "POST",
    url: "delete",
    dataType:   'json',
    async:      true,
    data: {delete: order},
  })
});

function avg(point, car){
  console.log(point[0].value, car[0].value);
  $.ajax({
    method: "POST",
    url: "/avg",
    dataType: 'json',
    async: true,
    data: {getAvgPoint: point[0].value, getAvgCar: car[0].value},
    success: function (count) {
      count = parseFloat(count);
      var progress = $('.progress-bar');
      if (!isNaN(count)){
        progress.css('width', (count*100/10) + '%');
        count = String(count);
        var text ='', lastNumber = +count[count.length-1];
        if (lastNumber >= 5) {
          text = ' Дней';
        }
        else{
          if (lastNumber == 1) {
            text = ' День';
          }
          else {
            text = ' Дня';
          }
        }
          progress[0].innerText = count + text;
        progress.css('color', 'white');
      }
      else {
        progress.css('width', 0 + '%');
        progress.css('color', 'black');
        progress[0].innerText = 0 + ' Дней';
      }
    }
  });
}

//Изменение точки по среднему продолжительности проката автомобиля
$('select[name="avgPoint"]').change(function () {
  avg($(this), $('select[name="avgCar"]'));
});
$('select[name="avgCar"]').change(function () {
  avg( $('select[name="avgPoint"]'), $(this));
});
$( document ).ready(function() {
    var selectPoint = $('select[name="avgPoint"]');
    if(selectPoint.length > 0){
      var selectCar = $('select[name="avgCar"]');
      avg(selectPoint.children(':first-child + option'), selectCar.children(':first-child + option'));
    }
});