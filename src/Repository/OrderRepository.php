<?php

namespace App\Repository;

use App\Entity\Orders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Orders|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orders|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orders[]    findAll()
 * @method Orders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Orders::class);
    }

    /*
     * Получение всх аренд
     */
    public function getAllOrders(){
        return $this->createQueryBuilder('orders')
            ->select('orders.id_order, car.car_name, car.number_car, us.fio as user_fio, us.pasport_number,
             orders.drive_start, orders.drive_end, orders.price, p_out.point_name AS point_start, p_out.adress as point_start_adr,
              p_in.point_name AS point_end,p_in.adress as point_end_adr')
            ->LeftJoin('App:Car','car', 'WITH', 'orders.id_car = car.id_car')
            ->LeftJoin('App:Users','us', 'WITH', 'orders.id_user = us.id_user')
            ->LeftJoin('App:Points','p_out', 'WITH',
                'orders.id_point_start = p_out.id_point')
            ->LeftJoin('App:Points','p_in', 'WITH',
                'orders.id_point_end = p_in.id_point')
            ->orderBy('orders.id_order', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /*
     * Получение аренды при изменении товара
     */
    public function getOrder($id){
        return $this->createQueryBuilder('orders')
            ->select('car.car_name, car.number_car, us.fio as user_fio, us.pasport_number, orders.drive_start,
                orders.drive_end, orders.price, p_out.point_name AS point_start,p_out.adress as point_start_adr,
                 p_in.point_name AS point_end,p_in.adress as point_end_adr')
            ->LeftJoin('App:Car','car', 'WITH', 'orders.id_car = car.id_car')
            ->LeftJoin('App:Users','us', 'WITH', 'orders.id_user = us.id_user')
            ->LeftJoin('App:Points','p_out', 'WITH',
                'orders.id_point_start = p_out.id_point')
            ->LeftJoin('App:Points','p_in', 'WITH',
                'orders.id_point_end = p_in.id_point')
            ->where('orders.id_order = '.$id)
            ->orderBy('orders.id_order', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /*
   * Получение аренды при изменении товара
   */
    public function getFilterOrder($car_number, $pas_num, $date_start, $date_end){
        return $this->createQueryBuilder('orders')
            ->select("orders.id_order, car.car_name, car.number_car, us.fio as user_fio, us.pasport_number,
             DATE_FORMAT(orders.drive_start, '%d.%m.%Y %H:%i') as drive_start, DATE_FORMAT(orders.drive_end, '%d.%m.%Y %H:%i') as drive_end,
              orders.price, p_out.point_name AS point_start, p_out.adress as point_start_adr,
              p_in.point_name AS point_end,p_in.adress as point_end_adr")
            ->LeftJoin('App:Car','car', 'WITH', 'orders.id_car = car.id_car')
            ->LeftJoin('App:Users','us', 'WITH', 'orders.id_user = us.id_user')
            ->LeftJoin('App:Points','p_out', 'WITH','orders.id_point_start = p_out.id_point')
            ->LeftJoin('App:Points','p_in', 'WITH','orders.id_point_end = p_in.id_point')
            ->where('us.pasport_number LIKE :pas_num',
                "car.number_car LIKE :car_number",
                "orders.drive_start LIKE :date_start",
                "orders.drive_end LIKE :date_end")
            ->setParameter('pas_num','%'.$pas_num.'%')
            ->setParameter('car_number','%'.$car_number.'%')
            ->setParameter('date_start','%'.$date_start.'%')
            ->setParameter('date_end','%'.$date_end.'%')
            ->orderBy('orders.id_order', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
