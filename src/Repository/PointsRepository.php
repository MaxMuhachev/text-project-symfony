<?php

namespace App\Repository;

use App\Entity\Points;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Points|null find($id, $lockMode = null, $lockVersion = null)
 * @method Points|null findOneBy(array $criteria, array $orderBy = null)
 * @method Points[]    findAll()
 * @method Points[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PointsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Points::class);
    }

    /*
      * Получение среднего количества дней проката по точке
      */
    public function getAvgPoint($point, $carNumber){
        return $this->createQueryBuilder('point')
            ->select('AVG(DATE_DIFF(orders.drive_end, orders.drive_start)) as sred ')
            ->Join('App:Orders','orders', 'WITH', 'orders.id_point_start = point.id_point')
            ->Join('App:Car','car', 'WITH', 'orders.id_car = car.id_car')
            ->where("point.adress='".$point."'", "car.number_car='".$carNumber."'")
            ->getQuery()
            ->getResult();
    }
}
