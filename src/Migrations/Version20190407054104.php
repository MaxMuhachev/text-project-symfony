<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190407054104 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users ADD user_pasport_number VARCHAR(10) NOT NULL, ADD user_pasport_date DATE NOT NULL, ADD user_pasport_who VARCHAR(70) NOT NULL, ADD user_pasport_adress VARCHAR(50) NOT NULL, CHANGE id_user id_user INT AUTO_INCREMENT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP user_pasport_number, DROP user_pasport_date, DROP user_pasport_who, DROP user_pasport_adress, CHANGE id_user id_user INT UNSIGNED AUTO_INCREMENT NOT NULL');
    }
}
