<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PointsRepository")
 */
class Points
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id_point;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $point_name;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $adress;

    public function getPointId(): ?int
    {
        return $this->id_point;
    }

    public function getPointName()
    {
        return $this->point_name;
    }

    public function getPointAdr()
    {
        return $this->adress;
    }

    public function setId($id_point)
    {
        $this->id_point = $id_point;

        return $this;
    }

    public function setPointName($point_name)
    {
        $this->point_name = $point_name;

        return $this;
    }

    public function setPointAdr($adress)
    {
        $this->adress = $adress;

        return $this;
    }
}
