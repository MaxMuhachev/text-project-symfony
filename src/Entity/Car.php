<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CarRepository")
 */
class Car
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id_car;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $car_name;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $number_car;

    public function getCarId(){
        return $this->id_car;
    }

    public function getCarName(){
        return $this->car_name;
    }

    public function getCarNumber(){
        return $this->number_car;
    }

    public function setCarId($id_car){

        $this->id_car = $id_car;

        return $this;
    }

    public function setCarName($car_name){

        $this->car_name = $car_name;

        return $this;
    }

    public function setCarNumber($number_car){

        $this->number_car = $number_car;

        return $this->number_car;
    }

}
