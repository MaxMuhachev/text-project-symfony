<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id_user;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $fio;

    /**
     * @ORM\Column(type="date")
     */
    private $date_born;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $pasport_number;

    /**
     * @ORM\Column(type="date")
     */
    private $pasport_date;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $pasport_who;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $pasport_city;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $pasport_adress;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $pasport_regesration;

    public function getUserId(): ?int
    {
        return $this->id_user;
    }

    public function getUserFio()
    {
        return $this->fio;
    }

    public function getUserDateBorn()
    {
        return $this->date_born;
    }

    public function getUserPasNumber(): ?string
    {
        return $this->pasport_number;
    }

    public function getUserPasDate()
    {
        return $this->pasport_date;
    }

    public function getUserPasWho(): ?string
    {
        return $this->pasport_who;
    }

    public function getUserPasCity(): ?string
    {
        return $this->pasport_city;
    }

    public function getUserPasAdr(): ?string
    {
        return $this->pasport_adress;
    }

    public function getUserPasReg(): ?string
    {
        return $this->pasport_regesration;
    }

    public function setUserId($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function setUserFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    public function setUserDateBorn($date_born)
    {
        $this->date_born = $date_born;

        return $this;
    }
    public function setUserPasNumber($pasport_number)
    {
        $this->pasport_number = $pasport_number;

        return $this;
    }

    public function setUserPasDate($pasport_date)
    {
        $this->pasport_date = $pasport_date;

        return $this;
    }

    public function setUserPasWho($pasport_who)
    {
        $this->pasport_who = $pasport_who;

        return $this;
    }

    public function setUserPasCity($pasport_city)
    {
        $this->pasport_city = $pasport_city;

        return $this;
    }

    public function setUserPasAdr($pasport_adress)
    {
        $this->pasport_adress = $pasport_adress;

        return $this;
    }

    public function setUserPasReg($pasport_regesration)
    {
        $this->pasport_regesration = $pasport_regesration;

        return $this;
    }
}
