<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 */
class Orders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id_order;

    /**
     * @ORM\Column(type="integer")
     */

    private $id_car;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $drive_start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $drive_end;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_point_start;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_point_end;

    public function getOrderId(): ?int
    {
        return $this->id_order;
    }

    public function getIdCar(): ?int
    {
        return $this->id_car;
    }

    public function getIdUser(): ?int
    {
        return $this->id_user;
    }

    public function getDriveStart()
    {
        return $this->drive_start;
    }

    public function getDriveEnd()
    {
        return $this->drive_end;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function getIdPointOut(): ?int
    {
        return $this->id_point_start;
    }

    public function getIdPointIn(): ?int
    {
        return $this->id_point_end;
    }

    public function setOrderId($id_order)
    {
        $this->id_order = $id_order;

        return $this;
    }

    public function setIdCar($id_car)
    {
        $this->id_car = $id_car;

        return $this;
    }

    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function setDriveStart($drive_start)
    {
        $this->drive_start = $drive_start;

        return $this;
    }

    public function setDriveEnd($drive_end)
    {
        $this->drive_end = $drive_end;

        return $this;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function setIdPointStart($id_point_start)
    {
        $this->id_point_start = $id_point_start;

        return $this;
    }

    public function setIdPointEnd($id_point_end)
    {
        $this->id_point_end = $id_point_end;

        return $this;
    }
}
