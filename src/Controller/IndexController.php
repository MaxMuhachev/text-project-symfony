<?php

/**
 * Фильр для аренды + вывод + добавление + удаление
 */

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Orders;
use App\Entity\Points;
use App\Entity\Users;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /*
     * Фильтр аренды
     */

    /**
     * Matches /
     *
     * @Route("/" , name="app_rent" )
     */

    public function getFiltreOrder(Request $request)
    {

        // Получаем все аренды автомобиля
        $orders = $this->getDoctrine()->getRepository(Orders::class)->getAllOrders();

        $points = $this->getDoctrine()->getRepository(Points::class)->findAll();

        $cars = $this->getDoctrine()->getRepository(Car::class)->findAll();
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();

        //Если есть запрос на фильтр
        if ($request->isXmlHttpRequest() && $request->request->get('getOrders') == 1) {

            $jsonData = array();
            // Получаем фильтры из формы
            $brandForm = $request->request->get('brand');
            $pasNumberForm = $request->request->get('fio');
            $drive_startForm = $request->request->get('date_start');
            $drive_endForm = $request->request->get('date_end');

            $drive_startForm = ($drive_startForm !== NULL) ? $drive_startForm : '';
            $drive_endForm = ($drive_endForm !== NULL) ?  date('Y-m-d', strtotime($drive_endForm)) : '';
            $brandForm = ($brandForm !== NULL) ?  $brandForm : '';
            $pasNumberForm = ($pasNumberForm !== NULL) ? $pasNumberForm : '';

            $orders = $this->getDoctrine()->getRepository(Orders::class)->getFilterOrder($brandForm, $pasNumberForm, $drive_startForm, $drive_endForm);
            return new JsonResponse($orders);
        } else {
            return $this->render('index.html.twig', [
                'orders' => $orders,
                'points' => $points,
                'users' => $users,
                'cars' => $cars,
            ]);
        }
    }

    /*
    * Получение среднего количества дней по точке
    */

    /**
     * Matches /avg
     *
     * @Route("/avg" , name="avgPoint" )
     */

    public function getAvgPont(Request $request)
    {
        //Если есть запрос
        if ($request->isXmlHttpRequest() && $request->request->get('getAvgPoint') != NULL) {
            $point = $this->getDoctrine()->getRepository(Points::class)->getAvgPoint(addslashes($request->request->get('getAvgPoint')),
                addslashes($request->request->get('getAvgCar')));
            return new JsonResponse($point[0]['sred']);
        }
        else{
            header('Location: ../');
        }
    }

    /*
     * Добавление аренды
     */

    /**
     * Matches orders/add
     *
     * @Route("orders/add", name="orderAdd")
     */

    public function addOrder(Request $request, $order = 0)
    {
        $cars = $this->getDoctrine()->getRepository(Car::class)->findAll();
        $points = $this->getDoctrine()->getRepository(Points::class)->findAll();
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();
        if (isset($_POST['submit'])){

            if ($request->request->get('date_start') < $request->request->get('date_end')){

                // Для добавления в БД
                $entityManager = $this->getDoctrine()->getManager();

                $car = $this->getDoctrine()->getRepository(Car::class)->findOneBy([
                    'car_name' => $request->request->get('brand'),

                ]);

                $user = $this->getDoctrine()->getRepository(Users::class)->findOneBy([
                    'pasport_number' => addslashes($request->request->get('name'))
                ]);

                // Добавляем аренду
                $order = !is_object($order) ?  new Orders() : $order;
                $order->setIdUser($user->getUserId());
                $order->setIdCar($car->getCarId());
                $order->setDriveStart(new \DateTime($request->request->get('date_start')." "
                    .$request->request->get('hour_out').":".$request->request->get('min_out')));
                $order->setDriveEnd(new \DateTime($request->request->get('date_end')." "
                    .$request->request->get('hour_in').":".$request->request->get('min_in')));
                $order->setPrice($request->request->get('price'));
                $order->setIdPointStart( $this->getDoctrine()->getRepository(Points::class)->findOneBy([
                    'point_name' => $request->request->get('point_out')
                ])->getPointId());
                $order->setIdPointEnd( $this->getDoctrine()->getRepository(Points::class)->findOneBy([
                    'point_name' => $request->request->get('point_in')
                ])->getPointId());

                // Добавляем в базу данных
                $entityManager->persist($order);
                $entityManager->flush();

                return $this->render('orders/changeOrder.html.twig', [
                    'cars' => $cars,
                    'error' => 'Аренда добавлена!',
                    'points' => $points,
                    'users' => $users,
                    'order' => $order,
                ]);
            }
            else{
                $order = [
                    'surname' => $request->request->get('surname'),
                    'car_name' => $request->request->get('name'),
                    'patronymic' => $request->request->get('patronymic'),
                    'drive_start' => $request->request->get('date_start'),
                    'drive_end' => $request->request->get('date_end'),
                    'price' => $request->request->get('price'),
                    'point_start' => $request->request->get('point_start'),
                    'point_end' => $request->request->get('point_end'),
                    'user_fio' => $request->request->get('user_fio'),
                ];
                return $this->render('orders/changeOrder.html.twig', [
                    'cars' => $cars,
                    'error' => '"Дата взятия в прокат" не может быть больше "Даты возврата"! Дата взятия: '
                    .$request->request->get('date_start').' Дата отдачи: '
                    .$request->request->get('date_end'),
                    'order' => $order,
                    'users' => $users,
                    'points' => $points,
                ]);
            }
        }
        else{
            return $this->render('orders/changeOrder.html.twig', [
                'cars' => $cars,
                'points' => $points,
                'users' => $users,
            ]);
        }
    }

    /*
     * Изменение арендами
     */

    /**
     * Matches /?id=(\d)*
     *
     * @Route("/orders/change", name="orderChange")
     */

    public function changeOrder(Request $request)
    {

        $orderId = $this->SetInt($request->query->get('rent'));
        $cars = $this->getDoctrine()->getRepository(Car::class)->findAll();
        $points = $this->getDoctrine()->getRepository(Points::class)->findAll();
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();
        $error ='';
        if (isset($_POST['submit']) && $orderId > 0) {
            $order = $this->getDoctrine()
                ->getRepository(Orders::class)
                ->find($orderId);
            $this->addOrder($request, $order);
            $error = 'Аренда изменена!';
        }
        $order = $this->getDoctrine()->getRepository(Orders::class)->getOrder($orderId);
        if (empty($order)){
            throw $this->createNotFoundException('Такая аренда не была найдена!');
        }
        return $this->render('orders/changeOrder.html.twig', [
            'cars' => $cars,
            'points' => $points,
            'order' => $order[0],
            'users' => $users,
            'error' => $error
        ]);

    }

    /*
     * Удаление аренды
     */
    /**
     * Matches
     *
     * @Route("/orders/delete", name="orderDelete")
     */
    public function deleteOrder(Request $request)
    {
        if ($request->isXmlHttpRequest() || $request->request->get('delete') !== null){
            $orderId = $this->SetInt($request->request->get('delete'));
            $order = $this->getDoctrine()
                ->getRepository(Orders::class)
                ->find($orderId);
            // Для удаления из БД
            $entityManager = $this->getDoctrine()->getManager();
            // Удаляем из базы данных
            $entityManager->remove($order);
            $entityManager->flush();
        }
        else{
            header('Location: ../');
        }
        return new  JsonResponse();
    }

    /*
     * Таблица с арендами
     */

    /**
     *
     * @Route("/orders/", name="ordersGet")
     */
    public function getOrders(Request $request)
    {

        // Получаем все аренды автомобиля
        $orders = $this->getDoctrine()->getRepository(Orders::class)->getAllOrders();

        return $this->render('orders/orders.html.twig', [
            'orders' => $orders,
        ]);
    }

    // Установка значаение в int
    public static function  setInt($id){
        settype($id, 'integer');
        return $id;
    }
}