<?php
namespace App\Controller;

use App\Entity\Users;
use App\Controller\IndexController as index;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UsersController extends AbstractController
{
    /*
     * Таблица с арендаторами
     */

    /**
     *
     * @Route("/users/", name="getUsers")
     */
    public function getUsers(Request $request)
    {
        // Получаем всех арендаторов
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();

        return $this->render('users/users.html.twig', [
            'users' => $users,
        ]);
    }

    /*
    * Изменение арендатора
    */

    /**
     *
     * @Route("/users/add", name="addUsers")
     */
    public function addUser(Request $request, $user = 0)
    {

        // Получаем все аренды автомобиля
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();


        if (isset($_POST['submit'])){

            if ($request->request->get('date_born') < (new \DateTime('+100 year'))->format('Y-m-d')
                && $request->request->get('date_pas') < (new \DateTime('+100 year'))->format('Y-m-d')){

                // Для добавления в БД
                $entityManager = $this->getDoctrine()->getManager();

                $fio = addslashes($request->request->get('surname')." ".$request->request->get('name')." "
                    .$request->request->get('patronymic'));

                // Добавляем пользователя
                $user = !is_object($user) ?  new Users() : $user;
                $user->setUserFio($fio);
                $user->setUserDateBorn(new \DateTime($request->request->get('date_born')));
                $user->setUserPasNumber($request->request->get('pas_serial').' '.$request->request->get('pas_number'));



                $user->setUserPasDate(new \DateTime($request->request->get('date_pas')));
                $user->setUserPasWho(addslashes($request->request->get('pas_who')));
                $user->setUserPasCity(addslashes($request->request->get('city')));
                $user->setUserPasAdr(addslashes(($request->request->get('adress'))));
                $user->setUserPasReg(addslashes($request->request->get('registrate')));

                $carParseNumber = $this->getDoctrine()
                    ->getRepository(Users::class)
                    ->findBy([
                        'pasport_number' => $user->getUserPasNumber(),
                    ]);

                if (empty($carParseNumber)){
                    // Добавляем в базу данных
                    $entityManager->persist($user);
                    $entityManager->flush();
                }
                else{
                    return $this->render('users/changeUser.html.twig', [
                        'error' => 'Такая серия и номер паспорта уже существует!',
                        'user' => $user
                    ]);
                }

                return $this->render('users/changeUser.html.twig', [
                    'success' => 'Арендатор добавлен!',
                ]);
            }
            else{
                return $this->render('users/changeUser.html.twig', [
                    'error' => 'Арендатору не должно быть больше 100 лет! ',
                    'user' =>[
                        'getUserFio' => $request->request->get('surname').' '
                            .$request->request->get('name').' '
                            .$request->request->get('patronymic'),
                        'getUserDateBorn' => $request->request->get('date_born'),
                        'getUserPasDate' => $request->request->get('date_pas'),
                        'getUserPasNumber' => $request->request->get('pas_serial').' '
                            .$request->request->get('pas_number'),
                        'getUserPasWho' => $request->request->get('pas_who'),
                        'getUserPasCity' => $request->request->get('city'),
                        'getUserPasAdr' => $request->request->get('street'),
                        'getUserPasReg' => $request->request->get('home'),
                        'kv' => $request->request->get('kv'),
                        'registrate' => $request->request->get('registrate'),
                    ]
                ]);
            }
        }
        else{
            return $this->render('users/changeUser.html.twig');
        }
    }



    /*
    * Изменение арендатора
    */

    /**
     * Matches /users/change/?id=(\d)*
     *
     * @Route("/users/change", name="userChange")
     */

    public function changeUser(Request $request)
    {
        $userId = index::setInt($request->query->get('user'));
        $success = '';
        $user = $this->getDoctrine()
            ->getRepository(Users::class)
            ->find($userId);
        if (isset($_POST['submit']) && $userId > 0) {
            $this->addUser($request, $user);
            $success = 'Арендатор изменён!';
        }
        return $this->render('users/changeUser.html.twig', [
            'user' => $user,
            'success' => $success
        ]);
    }

    /*
   * Удаление арендатора
   */
    /**
     * @Route("/users/delete", name="userDelete")
     */
    public function deleteUser1(Request $request)
    {
        if ($request->isXmlHttpRequest() || $request->request->get('delete') !== null){
            $userId = index::setInt($request->request->get('delete'));
            $user = $this->getDoctrine()
                ->getRepository(Users::class)
                ->find($userId);
            // Для удаления из БД
            $entityManager = $this->getDoctrine()->getManager();
            // Удаляем из базы данных
            $entityManager->remove($user);
            $entityManager->flush();
        }
        else{
            header('Location: ../');
        }
        return new  JsonResponse();
    }
}