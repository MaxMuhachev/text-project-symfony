<?php
namespace App\Controller;

use App\Controller\IndexController as index;
use App\Entity\Points;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PointsController extends AbstractController
{
    /*
     * Таблица с точками аренды
     */

    /**
     *
     * @Route("/points/", name="getPoint")
     */
    public function getPoints(Request $request)
    {
        // Получаем всех арендаторов
        $points = $this->getDoctrine()->getRepository(Points::class)->findAll();

        return $this->render('points/points.html.twig', [
            'points' => $points,
        ]);
    }

    /*
    * Изменение точки аренды
    */

    /**
     *
     * @Route("/points/add", name="addPoint")
     */
    public function addPoint(Request $request, $point = 0)
    {

        if (isset($_POST['submit'])){
            // Для добавления в БД
            $entityManager = $this->getDoctrine()->getManager();

            // Добавляем пользователя
            $point = !is_object($point) ?  new Points() : $point;
            $point->setPointName(addslashes(trim($request->request->get('name'))));
            $point->setPointAdr(addslashes(trim($request->request->get('adress'))));


            $pointParseName = $this->getDoctrine()
                ->getRepository(Points::class)
                ->findBy([
                    'point_name' => $point->getPointName(),
                ]);
            $pointParseAdr = $this->getDoctrine()
                ->getRepository(Points::class)
                ->findBy([
                    'adress' => $point->getPointAdr()
                ]);
            if (empty($pointParseName) && empty($pointParseAdr)){
                // Добавляем в базу данных
                $entityManager->persist($point);
                $entityManager->flush();
            }
            else{
                return $this->render('points/changePoint.html.twig', [
                    'error' => 'Такой салон уже существует!',
                    'point' => $point
                ]);
            }
            return $this->render('points/changePoint.html.twig', [
                'success' => 'Салон добавлен!',
                'point' => $point,
            ]);
        }
        else{
            return $this->render('points/changePoint.html.twig');
        }
    }



    /*
    * Изменение точки аренды
    */

    /**
     * Matches /points/change/?id=(\d)*
     *
     * @Route("/points/change", name="pointChange")
     */

    public function changePoint(Request $request)
    {
        $pointId = index::setInt($request->query->get('point'));
        $success = '';
        $point = $this->getDoctrine()
            ->getRepository(Points::class)
            ->find($pointId);
        if (!$point){
            throw $this->createNotFoundException('Такой салон не был найден!');
        }
        if (isset($_POST['submit']) && $pointId > 0) {
            $this->addPoint($request, $point);
            $success = 'Салон изменён!';
        }
        return $this->render('points/changePoint.html.twig', [
            'success' => $success,
            'point' => $point
        ]);
    }

    /*
   * Удаление точки аренды
   */
    /**
     * Matches /points/delete
     *
     * @Route("/points/delete", name="piontDelete")
     */
    public function deletePoint(Request $request)
    {
        if ($request->isXmlHttpRequest() || $request->request->get('delete') !== null){
            $pointId = index::setInt($request->request->get('delete'));
            $point = $this->getDoctrine()
                ->getRepository(Points::class)
                ->find($pointId);
            // Для удаления из БД
            $entityManager = $this->getDoctrine()->getManager();
            // Удаляем из базы данных
            $entityManager->remove($point);
            $entityManager->flush();
        }
        else{
            header('Location: ../');
        }
        return new  JsonResponse();
    }
}