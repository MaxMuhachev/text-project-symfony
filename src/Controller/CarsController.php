<?php
namespace App\Controller;

use App\Entity\Car;
use App\Controller\IndexController as index;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CarsController extends AbstractController
{
    /*
     * Таблица с машинами
     */

    /**
     *
     * @Route("/cars/", name="getCars")
     */
    public function getCars(Request $request)
    {
        // Получаем всех арендаторов
        $cars = $this->getDoctrine()->getRepository(Car::class)->findAll();

        return $this->render('cars/cars.html.twig', [
            'cars' => $cars,
        ]);
    }

    /*
    * Добавление машины
    */

    /**
     *
     * @Route("/cars/add", name="addCars")
     */
    public function addCar(Request $request, $car = 0)
    {

        if (isset($_POST['submit'])){
            // Для добавления в БД
            $entityManager = $this->getDoctrine()->getManager();

            // Добавляем пользователя
            $car = !is_object($car) ?  new Car() : $car;
            $car->setCarName(addslashes($request->request->get('brand')));
            $car->setCarNumber($request->request->get('number'));

            $carParseNumber = $this->getDoctrine()
                ->getRepository(Car::class)
                ->findBy([
                    'number_car' => $car->getCarNumber(),
                ]);

            if (empty($carParseNumber)){
                // Добавляем в базу данных
                $entityManager->persist($car);
                $entityManager->flush();
            }
            else{
                return $this->render('cars/changeCar.html.twig', [
                    'error' => 'Такая машина уже существует!',
                    'car' => $car
                ]);
            }

            return $this->render('cars/changeCar.html.twig', [
                'success' => 'Машина добавлена!',
            ]);
        }
        else{
            return $this->render('cars/changeCar.html.twig');
        }
    }


    /*
    * Изменение машины
    */

    /**
     * Matches /cars/change/?id=(\d)*
     *
     * @Route("/cars/change", name="carsChange")
     */

    public function changeCar(Request $request)
    {
        $carId = index::setInt($request->query->get('car'));
        $success ='';
        $car = $this->getDoctrine()
            ->getRepository(Car::class)
            ->find($carId);
        if (!$car){
            throw $this->createNotFoundException('Такая машина не была найдена!');
        }
        if (isset($_POST['submit']) && $carId > 0) {
            $this->addCar($request, $car);
            $success = 'Машина изменена!';
        }
        return $this->render('cars/changeCar.html.twig', [
            'success' => $success,
            'car' => $car
        ]);
    }

    /*
   * Удаление машины
   */
    /**
     * Matches /cars/delete
     *
     * @Route("/cars/delete", name="carDelete")
     */
    public function deleteCar(Request $request)
    {
        if ($request->isXmlHttpRequest() || $request->request->get('delete') !== null){
            $carId = index::setInt($request->request->get('delete'));
            $car = $this->getDoctrine()
                ->getRepository(Car::class)
                ->find($carId);
            if (!$car){
                throw $this->createNotFoundException('Такая машина не была найдена!');
            }
            // Для удаления из БД
            $entityManager = $this->getDoctrine()->getManager();
            // Удаляем из базы данных
            $entityManager->remove($car);
            $entityManager->flush();
        }
        else{
            header('Location: ../');
        }
        return new  JsonResponse();
    }
}