-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 08 2019 г., 21:03
-- Версия сервера: 5.7.24
-- Версия PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `rent`
--

-- --------------------------------------------------------

--
-- Структура таблицы `car`
--

DROP TABLE IF EXISTS `car`;
CREATE TABLE IF NOT EXISTS `car` (
  `id_car` int(11) NOT NULL AUTO_INCREMENT,
  `car_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_car` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_car`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `car`
--

INSERT INTO `car` (`id_car`, `car_name`, `number_car`) VALUES
(1, 'Toyota C-HR', 'Г332ББ159'),
(2, 'Toyota Fortuner', 'А217КБ159'),
(3, 'Toyota Camry', 'Г265ЛК59'),
(4, 'Mazda 6', 'А336ПМ159'),
(5, 'Mazda CX-5', 'А552ТТ159'),
(6, 'Lexus NX', 'А223КИ159'),
(7, 'Lexus LS', 'Ф478АМ59'),
(8, 'Lexus IS', 'С858ДА159');

-- --------------------------------------------------------

--
-- Структура таблицы `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190327180847', '2019-03-27 18:16:14'),
('20190328033009', '2019-03-28 17:48:55'),
('20190328174808', '2019-03-28 17:48:55'),
('20190328175340', '2019-03-28 17:54:24'),
('20190330044223', '2019-04-07 05:42:18');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_car` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `drive_start` datetime NOT NULL,
  `drive_end` datetime NOT NULL,
  `price` int(11) NOT NULL,
  `id_point_start` int(11) NOT NULL,
  `id_point_end` int(11) NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `id_point_start` (`id_point_start`),
  KEY `id_point_end` (`id_point_end`),
  KEY `id_user` (`id_user`),
  KEY `orders_ibfk_1` (`id_car`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id_order`, `id_car`, `id_user`, `drive_start`, `drive_end`, `price`, `id_point_start`, `id_point_end`) VALUES
(4, 5, 4, '2019-03-06 12:00:00', '2019-03-08 22:00:00', 2000, 3, 1),
(5, 4, 6, '2019-02-19 11:00:00', '2019-03-26 16:30:00', 12000, 2, 1),
(6, 8, 6, '2019-03-09 14:00:00', '2019-03-10 13:28:00', 4000, 4, 4),
(7, 7, 7, '2019-01-17 15:15:00', '2019-01-22 14:32:00', 5500, 4, 3),
(8, 1, 9, '2019-03-16 12:00:00', '2019-03-22 10:26:00', 500, 2, 1),
(11, 4, 10, '2019-03-13 10:00:00', '2019-03-14 18:51:00', 1200, 3, 1),
(12, 1, 10, '2019-03-17 08:00:00', '2019-03-18 09:32:00', 12000, 1, 2),
(16, 5, 11, '2019-04-01 07:30:00', '2019-04-02 10:10:00', 1200, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `points`
--

DROP TABLE IF EXISTS `points`;
CREATE TABLE IF NOT EXISTS `points` (
  `id_point` int(11) NOT NULL AUTO_INCREMENT,
  `point_name` varchar(30) NOT NULL,
  `adress` varchar(30) NOT NULL,
  PRIMARY KEY (`id_point`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `points`
--

INSERT INTO `points` (`id_point`, `point_name`, `adress`) VALUES
(1, 'Мираж', 'Спешилова,77'),
(2, 'Люкс', 'Свиязева,43'),
(3, 'Umbrella', 'Ижевская,25'),
(4, 'Fast Car', 'Аэродромная,1'),
(7, 'Виктория', 'Ленина,128');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(30) NOT NULL,
  `date_born` date DEFAULT NULL,
  `pasport_number` varchar(11) NOT NULL,
  `pasport_date` date NOT NULL,
  `pasport_who` varchar(80) NOT NULL,
  `pasport_city` varchar(30) NOT NULL,
  `pasport_adress` varchar(50) NOT NULL,
  `pasport_regesration` varchar(80) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id_user`, `fio`, `date_born`, `pasport_number`, `pasport_date`, `pasport_who`, `pasport_city`, `pasport_adress`, `pasport_regesration`) VALUES
(2, 'Кормщикова Марина Михайловна', '1998-11-22', '5864 445541', '2018-03-06', 'отделение полиции гор.Перми Индустриального района', 'Пермь', 'ул. Эпроновская, дом 15а, кв.5', 'отделение полиции гор.Перми Индустриального района'),
(3, 'Игнатьев Леонид Григорьевич', '1986-03-01', '5834 147964', '2015-12-09', 'отделение полиции гор.Перми Кировского района', 'Пермь', 'ул. Оборонщиков, дом 7, кв.33', 'отделение полиции гор.Перми Кировского района'),
(4, 'Медведев Дмитрий Александрович', '1991-09-18', '5971 145412', '2016-06-07', 'отделение полиции гор.Перми Ленинского района', 'Пермь', 'ул.Грузинская, дом 15, кв.6', 'отделение полиции гор.Перми Ленинского района'),
(5, 'Ощепков Роман Геннадьевич', '1975-06-25', '5942 451634', '2003-01-13', 'отделение полиции гор.Перми Мотовилихинского района', 'Пермь', 'ул. Боровая, дом 22, кв.5', 'отделение полиции гор.Перми Мотовилихинского района'),
(6, 'Дудкина Юлия Викторовна', '1989-12-21', '5318 122542', '2008-08-18', 'отделение полиции гор.Перми Ленинского района', 'Пермь', 'Пожарная, дом. 14/1, кв.15', 'отделение полиции гор.Перми Ленинского района'),
(7, 'Плотников Алена Николаевна', '1993-02-14', '5979 565564', '2005-11-20', 'отделение полиции гор.Перми Дзержинского района', 'Пермь', 'ул. Пушкина, дом 6, кв.18', 'отделение полиции гор.Перми Дзержинского района'),
(8, 'Анаеьев Михаил Алексеевич', '1997-05-10', '5914 223485', '2013-05-25', 'отделение полиции гор.Перми Индустриального района', 'Пермь', 'ул. Екатерининская, дом 165, кв.98', 'отделение полиции гор.Перми Индустриального района'),
(9, 'Анимпбодистова Вера Павловна', '1985-09-03', '5846 665412', '2014-12-31', 'отделение полиции гор.Перми Ленинского района', 'Пермь', 'Ижевская, дом 23, кв.22', 'отделение полиции гор.Перми Ленинского района'),
(10, 'Петров Николай Александрович', '1973-04-15', '5687 251546', '2003-09-15', 'отделение полиции гор.Перми Кировского района', 'Пермь', 'ул. Сергинская, дом 32,кв. 19', 'отделение полиции гор.Перми Кировского района'),
(11, 'Аймухамбетов Алевжан Борисович', '1991-07-08', '5984 554885', '2010-07-28', 'отделение полиции гор.Перми Мотовилихинского района', 'Пермь', 'ул. Лукоянова, дом 24,кв. 56', 'отделение полиции гор.Перми Мотовилихинского района'),
(12, 'Агаев Роман Гобилович', '1997-03-19', '5916 596484', '2017-09-13', 'Отделом УФМС России по Пермкому краю в Гайвенском районе гор. Перми', 'Пермь', 'ул.Захирова, дом 16, кв. 36', 'Отделом УФМС России по Пермкому краю в Гайвенском районе гор. Перми');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`id_car`) REFERENCES `car` (`id_car`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`id_point_start`) REFERENCES `points` (`id_point`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`id_point_end`) REFERENCES `points` (`id_point`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
